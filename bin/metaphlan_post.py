#!/usr/bin/env python3.11
import pandas as pd
import argparse
from pathlib import Path
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("sgbtaxo", help="SGB taxonomy (GTDB format)", default=Path("mpa_vOct22_CHOCOPhlAnSGB_202212_SGB2GTDB.tsv.gz"),
                    type=Path)
parser.add_argument("merged_profile_file", type=Path)
parser.add_argument("reads", type=Path)
parser.add_argument("output_count", type=Path)
parser.add_argument("output_taxo", type=Path)
args = parser.parse_args()

df = pd.read_csv(args.merged_profile_file, comment='#', sep="\t", header=0)
# Removing part before and including the underscore
# print(df.columns)
df.columns = ["_".join(col.split("_")[:-1]) if col.endswith("_metaphlan")  else col for col in df.columns]
# print(df.columns)
reads_df = pd.read_csv(args.reads, names=["Samples", "Reads"], sep="\t", index_col=0)
# print(df)
df["SGB"] = df["clade_name"].str.replace('t__', '')
del df["clade_name"]
for col in df.columns[1:]:
    if col == "SGB":
        continue
    df[col] *= reads_df.loc[col, 'Reads'] / 100
df.insert(0, "SGB", df.pop("SGB"))
df = df[df["SGB"] != "UNCLASSIFIED"]
df = df[df.select_dtypes(include=[np.number]).sum(axis=1) != 0]
df = df.loc[:, (df.sum(axis=0) != 0)]
df.to_csv(args.output_count, sep="\t", index=False)
taxo = pd.read_csv(args.sgbtaxo, compression="gzip", delimiter="\t", names=["SGB", "Annotation"])
taxo = taxo[taxo['SGB'].isin(df['SGB'])]
taxo['Annotation'] = taxo['Annotation'].str.replace(r'\w__', '', regex=True)
split_annotations = taxo['Annotation'].str.split(',').str[0].str.split('|', expand=True)
# Select only the first 7 columns
split_annotations = split_annotations.iloc[:, :7]
# print(split_annotations)
if split_annotations.shape[1] == 7:
    taxo[['Kingdom', 'Phylum', 'Class', 'Order', 'Family', 'Genus', 'Species']] =  split_annotations
    # Add 'marker_SGB' column
    taxo['marker_SGB'] = taxo['SGB'] + '_' + taxo['Annotation'].apply(lambda x: [i for i in x.split('|') if i][-1])
    del taxo['Annotation']
    taxo.to_csv(args.output_taxo, sep="\t", index=False)