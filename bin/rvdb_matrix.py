#!/usr/bin/env python3.11
import pandas as pd
import argparse
from pathlib import Path
from taxadb.taxid import TaxID
from taxadb.accessionid import AccessionID
from operator import itemgetter
import numpy as np
parser = argparse.ArgumentParser()
parser.add_argument("--taxadb", help="Taxadb databse", type=Path, required=True)
parser.add_argument('--cov',dest="cov", type=Path, nargs='+')
parser.add_argument('--counts', dest="counts", type=Path, nargs='+')
parser.add_argument('--om', dest='output_matrix', type=Path, default="matrix.tsv", help="Output contengency matrix")
# parser.add_argument('--ot', dest='output_taxo_table', type=Path, default="taxonomy.tsv", help="Output taxonomy")
args = parser.parse_args()

# Initialize an empty list to hold processed DataFrames
processed_dataframes = []
coverage_dataframes = []

for c in args.cov:
    dataframe = pd.read_csv(c, delimiter="\t", header=0)
    dataframe = dataframe.drop('startpos', axis=1)
    dataframe = dataframe.drop('endpos', axis=1)
    dataframe = dataframe.drop('numreads', axis=1)
    dataframe = dataframe.drop('covbases', axis=1)
    dataframe = dataframe.drop('meandepth', axis=1)
    dataframe = dataframe.drop('meanbaseq', axis=1)
    dataframe = dataframe.drop('meanmapq', axis=1)
    # Split the 'reference_name' column to extract 'accession_number'
    dataframe['#rname'] = dataframe['#rname'].str.split('|').str[2].str.split('.').str[0]
    dataframe[c.name.replace("_cov.tsv", "")] = dataframe['coverage']
    # Set the 'accession_number' column as the index of the DataFrame
    dataframe = dataframe.set_index('#rname')
    dataframe = dataframe.drop('coverage', axis=1)
    coverage_dataframes.append(dataframe)
    #rname	startpos	endpos	numreads	covbases	coverage	meandepth	meanbaseq	meanmapq

cov_combined = coverage_dataframes[0]
for dataframe in coverage_dataframes[1:]:
    cov_combined = cov_combined.merge(dataframe, left_index=True, right_index=True)
print(cov_combined)


# rows_mask = (cov_combined > 70).any(axis=1)
rows_mask = cov_combined > 70

# Loop through all files, process them, and add to the list
for f in args.counts:
    # Read the file into a DataFrame
    dataframe = pd.read_csv(f, delimiter="\t",
        names=["reference_name", "sequence_length", "mapped", "unmapped"])

    # Drop rows where 'reference_name' is '*'
    dataframe = dataframe.loc[dataframe['reference_name'] != '*']

    # Split the 'reference_name' column to extract 'accession_number'
    dataframe['accession_number'] = dataframe['reference_name'].str.split('|').str[2].str.split('.').str[0]

    # Set the 'accession_number' column as the index of the DataFrame
    dataframe = dataframe.set_index('accession_number')

    dataframe[f.name.replace(".tsv", "")] = dataframe['mapped'] / dataframe['sequence_length'] * 1000
    # Drop the now-unneeded 'reference_name' column
    dataframe = dataframe.drop('reference_name', axis=1)
    dataframe = dataframe.drop('unmapped', axis=1)
    dataframe = dataframe.drop('mapped', axis=1)
    dataframe = dataframe.drop('sequence_length', axis=1)

    # Add the processed DataFrame to the list
    processed_dataframes.append(dataframe)

# Merge all processed DataFrames
# Start with the first DataFrame and iteratively merge the rest
df_combined = processed_dataframes[0]
for dataframe in processed_dataframes[1:]:
    df_combined = df_combined.merge(dataframe, left_index=True, right_index=True)

print(rows_mask)
df_combined = df_combined[rows_mask]
print(df_combined)
# For testing :)
# df_combined = df_combined[df_combined.select_dtypes(include=[np.number]).sum(axis=1) >= 4]
# df_combined = df_combined.loc[:, (df_combined.sum(axis=0) >= 4)]


# df_combined = df_combined[df_combined.select_dtypes(include=[np.number]).sum(axis=1) >= 1]
df_combined = df_combined.loc[:, (df_combined.sum(axis=0) >= 10)]

tax_db = TaxID(dbtype="sqlite", dbname=args.taxadb)
accession_db = AccessionID(dbtype="sqlite", dbname=args.taxadb)
accession =df_combined.index.values.tolist()
ncbi = {}
chunks = [accession[i : i + 999] for i in range(0, len(accession), 999)]
for accession_chunk in chunks:
    taxids = accession_db.taxid(accession_chunk)
    for tax in taxids:
        lineage = tax_db.lineage_name(tax[1], reverse=True)
        # if tax[1] == 9606:
        if lineage[1] == "Eukaryota":
            df_combined.drop(tax[0], inplace=True)
        elif len(lineage)==16:
            ncbi[tax[0]] = itemgetter(0,5,9,11,12,13,15)(lineage)
        elif len(lineage)==15:
            ncbi[tax[0]] = itemgetter(0,5,8,9,11,12,14)(lineage)
        elif len(lineage)==14:
            ncbi[tax[0]] = itemgetter(0,5,8,9,11,12,13)(lineage)
        elif len(lineage)==13:
            ncbi[tax[0]] = itemgetter(0,5,7,8,9,10,12)(lineage)
        # elif lineage[1] == "Eukaryota" and len(lineage)==16:
        #     ncbi[tax[1]] = itemgetter(1,5,7,8,9,10,12)(lineage)
        elif len(lineage)==12:
            ncbi[tax[0]] = itemgetter(0,5,7,8,9,10,11)(lineage)
        elif len(lineage)==11:
            ncbi[tax[0]] = itemgetter(0,4,5,7,8,9,10)(lineage)
        elif len(lineage)==10:
            ncbi[tax[0]] = itemgetter(0,3,4,6,7,8,9)(lineage)
        elif len(lineage) == 9:
            ncbi[tax[0]] = itemgetter(0,2,3,4,6,7,8)(lineage)
        elif len(lineage) == 8:
            ncbi[tax[0]] = lineage[1:]
        elif len(lineage) == 7:
            ncbi[tax[0]] = lineage
        elif len(lineage) > 0 and len(lineage) < 7:
            ncbi[tax[0]] = lineage + ['']*(7-len(lineage))
        else:
            print(tax)
            print(lineage)
            # ncbi[tax[0]] = lineage
            print(len(lineage))

# df_combined.to_csv(args.output_matrix, sep="\t")
ncbi_df = pd.DataFrame(ncbi)
ncbi_df = ncbi_df.transpose()
ncbi_df.reset_index(level=0, inplace=True)
ncbi_df = ncbi_df.astype(str)
# Select only the first 7 columns
# ncbi_df = ncbi_df.iloc[:, :7]
ncbi_df.columns = ['Accession', 'Kingdom', 'Phylum', 'Class', 'Order', 'Family', 'Genus', 'Species']
# print(split_annotations)
# ncbi_df.to_csv(args.output_taxo_table, sep="\t", index=False)
ncbi_df = ncbi_df.set_index('Accession')
df_combined_with_annotations = df_combined.merge(ncbi_df[['Species']], left_index=True, right_index=True)
print(df_combined_with_annotations)
# Filter df_combined_with_annotations based on marker criteria
marker_threshold = 3
marker_fraction = 0.8
# Apply filter to ensure at least 80% of markers are > 0
df_filtered = df_combined_with_annotations[
    df_combined_with_annotations.drop(columns=['Species']).gt(marker_threshold).mean(axis=1) >= marker_fraction
]
df_averaged = df_filtered.groupby('Species').mean()
df_averaged = df_averaged.round(decimals=0)
df_averaged=df_averaged.fillna(0)
df_averaged = df_averaged.loc[(df_averaged != 0).any(axis=1), :]
df_averaged = df_averaged.loc[:, (df_averaged != 0).any(axis=0)]
df_averaged.to_csv(args.output_matrix, sep="\t", index=True)
# # Additional filtering based on the sum of counts
# df_combined_with_annotations = df_combined_with_annotations[df_combined_with_annotations.drop(columns=['Accession', 'Species']).select_dtypes(include=[np.number]).sum(axis=1) >= 10]
# df_combined_with_annotations = df_combined_with_annotations.loc[:, (df_combined_with_annotations.drop(columns=['Accession', 'Species']).sum(axis=0) >= 10)]

# # Optionally write the final filtered DataFrame to a file
# df_combined_with_annotations.to_csv(args.output_matrix, sep="\t")