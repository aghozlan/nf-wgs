#!/usr/bin/env python3.11
import pandas as pd
import argparse
from pathlib import Path
import gzip

# Set up argument parser
parser = argparse.ArgumentParser(description='Count reads in fastq files.')
parser.add_argument('files', metavar='F', type=Path, nargs='+',
                    help='a list of fastq files')
parser.add_argument('output', type=Path, help='Output results')

# Parse arguments
args = parser.parse_args()
fastq_files = args.files

# Initialize an empty list to store the counts
counts = []

# Iterate over the fastq files
for fastq_file in fastq_files:
    print(fastq_file)
    with gzip.open(fastq_file, 'r') as f:
        # Count the number of lines and divide by 4 to get the number of reads
        num_reads = sum(1 for _ in f) // 4
        counts.append(num_reads)

# Create a DataFrame
df = pd.DataFrame({'Sample_file': fastq_files, 'Reads': counts})
# Convert 'Sample_file' to string type
df['Sample_file'] = df['Sample_file'].astype(str)
# Remove '_R1.fastq.gz' from file names
df['Sample_file'] = df['Sample_file'].str.replace('_R1.fastq.gz', '')
df.to_csv(args.output, sep="\t", index=False)