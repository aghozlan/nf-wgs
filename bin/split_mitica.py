#!/usr/bin/env python3.11
import pandas as pd
import argparse
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("input_matrix_file", type=Path)
# parser.add_argument("taxonomy_matrix_file", type=Path)
parser.add_argument("target_matrix_file", type=Path)
parser.add_argument("output_dir", type=Path)
args = parser.parse_args()

# df_taxo = pd.read_csv(args.taxonomy_matrix_file, sep="\t", index_col='Genome_file')
df_target = pd.read_csv(args.target_matrix_file, sep=";", index_col='Sample')
df = pd.read_csv(args.input_matrix_file, sep="\t", index_col='Name')
milk_columns = [col for col in df.columns if 'L' in col]
pv_columns = [col for col in df.columns if 'PV' in col]
mc_columns = [col for col in df.columns if 'MC' in col]
ec_columns = [col for col in df.columns if 'EC' in col]
oral_columns = [col for col in df.columns if 'B' in col]

# Create DataFrame for 'L' samples
df_milk  = df[milk_columns]
df_milk = df_milk.loc[:, (df_milk.sum(axis=0) != 0)]
df_milk = df_milk.loc[(df.sum(axis=1) != 0)]
df_milk.to_csv(args.output_dir / "milk_count_matrix.tsv", sep="\t")
# df_taxo.loc[df_taxo.index.intersection(df_milk.index)].to_csv(args.output_dir / "milk_taxo_matrix.tsv", sep="\t")
df_target.loc[df_target.index.intersection(milk_columns)].to_csv(args.output_dir / "milk_target.tsv", sep="\t")

# Create DataFrame for 'PV' samples
df_pv  = df[pv_columns]
df_pv = df_pv.loc[:, (df_pv.sum(axis=0) != 0)]
df_pv = df_pv.loc[(df.sum(axis=1) != 0)]
df_pv.to_csv(args.output_dir / "vaginal_count_matrix.tsv", sep="\t")
# df_taxo.loc[df_taxo.index.intersection(df_pv.index)].to_csv(args.output_dir / "vaginal_taxo_matrix.tsv", sep="\t")
df_target.loc[df_target.index.intersection(pv_columns)].to_csv(args.output_dir / "vaginal_target.tsv", sep="\t")

# Create DataFrame for 'MC' samples
df_mc  = df[mc_columns]
df_mc = df_mc.loc[:, (df_mc.sum(axis=0) != 0)]
df_mc = df_mc.loc[(df.sum(axis=1) != 0)]
df_mc.to_csv(args.output_dir / "mother_fecal_count_matrix.tsv", sep="\t")
# df_taxo.loc[df_taxo.index.intersection(df_mc.index)].to_csv(args.output_dir / "mother_fecal_taxo_matrix.tsv", sep="\t")
df_target.loc[df_target.index.intersection(mc_columns)].to_csv(args.output_dir / "mother_fecal_target.tsv", sep="\t")

# Create DataFrame for 'EC' samples
df_ec  = df[ec_columns]
df_ec = df_ec.loc[:, (df_ec.sum(axis=0) != 0)]
df_ec = df_ec.loc[(df.sum(axis=1) != 0)]
df_ec.to_csv(args.output_dir / "child_fecal_count_matrix.tsv", sep="\t")
# df_taxo.loc[df_taxo.index.intersection(df_ec.index)].to_csv(args.output_dir / "child_fecal_taxo_matrix.tsv", sep="\t")
df_target.loc[df_target.index.intersection(ec_columns)].to_csv(args.output_dir / "child_fecal_target.tsv", sep="\t")

# Create DataFrame for 'B' samples
df_oral  = df[oral_columns]
df_oral = df_oral.loc[:, (df_oral.sum(axis=0) != 0)]
df_oral = df_oral.loc[(df.sum(axis=1) != 0)]
df_oral.to_csv(args.output_dir / "oral_count_matrix.tsv", sep="\t")
# df_taxo.loc[df_taxo.index.intersection(df_oral.index)].to_csv(args.output_dir / "oral_taxo_matrix.tsv", sep="\t")
df_target.loc[df_target.index.intersection(oral_columns)].to_csv(args.output_dir / "oral_target.tsv", sep="\t")