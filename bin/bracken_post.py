#!/usr/bin/env python3.11
import pandas as pd
import argparse
from pathlib import Path
import numpy as np
from taxadb.accessionid import AccessionID
from taxadb.taxid import TaxID
from operator import itemgetter

# create the top-level parser
parser = argparse.ArgumentParser()
parser.add_argument("--keepZeros", help="Do not remove taxid that have a null count in the final matrix",
                    action="store_false")
parser.add_argument("--taxadb", help="Taxadb database", type=Path)
parser.add_argument('bracken_agg', type=Path, nargs='+')
parser.add_argument('output_matrix', type=Path, default="matrix.tsv", help="Combine all bracken data into a contengency matrix")
parser.add_argument('output_taxo_table', type=Path, default="taxonomy.tsv", help="Output the taxonomy of existing SGB")
args = parser.parse_args()

accession_db = AccessionID(dbtype="sqlite", dbname=args.taxadb)
tax_db = TaxID(dbtype="sqlite", dbname=args.taxadb)

dataframes = []
for f in args.bracken_agg:
    a=pd.read_csv(f, delimiter="\t",  usecols=["taxonomy_id", "new_est_reads"], header=0)
    a.columns = ["taxonomy_id", f.name.replace(".bracken", "")]
    a= a.set_index('taxonomy_id')
    dataframes.append(a)

df = dataframes[0]
for dataframe in dataframes[1:]:
    # print(dataframe)
    df = pd.merge(df, dataframe, left_index=True, right_index=True, how='outer').fillna(0).astype(int)

# Remove rows with only zeros
if args.keepZeros:
    df = df[df.select_dtypes(include=[np.number]).sum(axis=1) != 0]
    df = df.loc[:, (df.sum(axis=0) != 0)]

df.to_csv(args.output_matrix, sep="\t")

taxids = df.index.tolist()
# accession=df["taxonomy_id"].tolist()
# chunks = [accession[i : i + 999] for i in range(0, len(accession), 999)]
ncbi = {}
# for accession_chunk in chunks:
#     taxids = accession_db.taxid(accession_chunk)
for tax in taxids:
    lineage = tax_db.lineage_name(tax, reverse=True)
    # print(lineage)
    if not lineage:
        pass
    elif tax not in ncbi:
        # print(lineage)
        if lineage[1] == "Eukaryota" and len(lineage)==13:
            ncbi[tax] = itemgetter(1,5,7,8,9,10,12)(lineage)
        # elif lineage[1] == "Eukaryota" and len(lineage)==16:
        #     ncbi[tax[1]] = itemgetter(1,5,7,8,9,10,12)(lineage)
        elif len(lineage)>=10:
            ncbi[tax] = itemgetter(1,2,3,4,6,7,9)(lineage)
        elif len(lineage) == 9:
            ncbi[tax] = itemgetter(1,2,3,4,6,7,8)(lineage)
        elif len(lineage) == 8:
            ncbi[tax] = lineage[1:]
        elif len(lineage) > 0:
            ncbi[tax] = lineage + ['']*(7-len(lineage))
ncbi_df = pd.DataFrame(ncbi)
ncbi_df_transposed = ncbi_df.transpose()
ncbi_df_transposed.reset_index(level=0, inplace=True)
ncbi_df_transposed = ncbi_df_transposed.astype(str)
# print(ncbi_df_transposed)
ncbi_df_transposed.columns = ['SGB', 'Kingdom', 'Phylum', 'Class', 'Order', 'Family', 'Genus', 'Species']
ncbi_df_transposed.to_csv(args.output_taxo_table, sep="\t", index=False)