#!/usr/bin/env python3.11
import pandas as pd
import argparse
from pathlib import Path
from operator import itemgetter
import numpy as np
parser = argparse.ArgumentParser()
parser.add_argument("--mgv_contig_info", help="MGV_contig_info", type=Path, required=True)
parser.add_argument('--cov',dest="cov", type=Path, nargs='+')
parser.add_argument('--counts', dest="counts", type=Path, nargs='+')
parser.add_argument('--om', dest='output_matrix', type=Path, default="matrix.tsv", help="Output contengency matrix")
# parser.add_argument('--ot', dest='output_taxo_table', type=Path, default="taxonomy.tsv", help="Output taxonomy")
args = parser.parse_args()

processed_dataframes = []
coverage_dataframes = []

for c in args.cov:
    dataframe = pd.read_csv(c, delimiter="\t", header=0)
    dataframe = dataframe.drop('startpos', axis=1)
    dataframe = dataframe.drop('endpos', axis=1)
    dataframe = dataframe.drop('numreads', axis=1)
    dataframe = dataframe.drop('covbases', axis=1)
    dataframe = dataframe.drop('meandepth', axis=1)
    dataframe = dataframe.drop('meanbaseq', axis=1)
    dataframe = dataframe.drop('meanmapq', axis=1)
    # Split the 'reference_name' column to extract 'accession_number'
    # print(dataframe)
    # dataframe['#rname'] = dataframe['#rname'].str.split('|').str[2].str.split('.').str[0]
    dataframe[c.name.replace("_cov.tsv", "")] = dataframe['coverage']
    # Set the 'accession_number' column as the index of the DataFrame
    dataframe = dataframe.set_index('#rname')
    dataframe = dataframe.drop('coverage', axis=1)
    coverage_dataframes.append(dataframe)
    #rname	startpos	endpos	numreads	covbases	coverage	meandepth	meanbaseq	meanmapq

cov_combined = coverage_dataframes[0]
for dataframe in coverage_dataframes[1:]:
    cov_combined = cov_combined.merge(dataframe, left_index=True, right_index=True)
print(cov_combined)

rows_mask = cov_combined > 70.0

# Loop through all files, process them, and add to the list
for f in args.counts:
    # Read the file into a DataFrame
    dataframe = pd.read_csv(f, delimiter="\t",
        names=["reference_name", "sequence_length", "mapped", "unmapped"])

    # Drop rows where 'reference_name' is '*'
    dataframe = dataframe.loc[dataframe['reference_name'] != '*']

    # Split the 'reference_name' column to extract 'accession_number'
    # dataframe['accession_number'] = dataframe['reference_name'].str.split('|').str[2].str.split('.').str[0]

    # Set the 'accession_number' column as the index of the DataFrame
    # dataframe = dataframe.set_index('accession_number')
    dataframe = dataframe.set_index('reference_name')

    dataframe[f.name.replace(".tsv", "")] = dataframe['mapped'] / dataframe['sequence_length'] * 1000
    # Drop the now-unneeded 'reference_name' column
    # dataframe = dataframe.drop('reference_name', axis=1)
    dataframe = dataframe.drop('unmapped', axis=1)
    dataframe = dataframe.drop('mapped', axis=1)
    dataframe = dataframe.drop('sequence_length', axis=1)

    # Add the processed DataFrame to the list
    processed_dataframes.append(dataframe)

# Merge all processed DataFrames
# Start with the first DataFrame and iteratively merge the rest
df_combined = processed_dataframes[0]
for dataframe in processed_dataframes[1:]:
    df_combined = df_combined.merge(dataframe, left_index=True, right_index=True)

print(rows_mask)
df_combined = df_combined[rows_mask]
df_combined = df_combined.loc[:, (df_combined.sum(axis=0) >= 10)]
print(df_combined)