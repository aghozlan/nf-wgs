#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html
import argparse
import pandas as pd
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("sylph_file", type=Path)
parser.add_argument("reads", type=Path)
parser.add_argument("sylph_db", type=Path)
parser.add_argument("output_count", type=Path)
parser.add_argument("output_taxo", type=Path)
args = parser.parse_args()

data = pd.read_csv(args.sylph_file, header=0, sep="\t")
data['Sample_file'] = data['Sample_file'].str.replace('cleaned_reads/', '')
data['Sample_file'] = data['Sample_file'].str.replace('_R1.fastq.gz', '')
data['Genome_file'] = data['Genome_file'].str.split('/').str[-1]
data['Genome_file'] = data['Genome_file'].str.split('_genomic').str[0]

# Read the reads.txt file into another DataFrame
reads_df = pd.read_csv(args.reads, header=0, sep="\t", index_col=0)
print(reads_df)
# Pivot the dataframe
pivot_df = data.pivot(index='Genome_file', columns='Sample_file', values='Taxonomic_abundance')
# Fill NaN values with 0 (assuming that NaN means the taxon was not found in the sample)
pivot_df = pivot_df.fillna(0)
for col in pivot_df.columns:
    pivot_df[col] *= reads_df.loc[col, 'Reads'] / 100

# Now, pivot_df contains the multiplied values
pivot_df.to_csv(args.output_count, sep="\t")

taxo = pd.read_csv(args.sylph_db, names=["Genome_file", "Annotation"], sep="\t")
taxo_filtered = taxo[taxo['Genome_file'].isin(data['Genome_file'])]
taxo_filtered['Annotation'] = taxo_filtered['Annotation'].str.replace(r'\w__', '', regex=True)
# Split 'Annotation' into multiple columns
taxo_filtered[['Domain', 'Phylum', 'Class', 'Order', 'Family', 'Genus', 'Species']] = taxo_filtered['Annotation'].str.split(';', expand=True)
del taxo_filtered['Annotation']
taxo_filtered.to_csv(args.output_taxo, sep="\t", index=False)