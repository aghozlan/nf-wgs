#!/usr/bin/env python3.11
import pandas as pd
import argparse
from pathlib import Path
import numpy as np
from taxadb.accessionid import AccessionID
from taxadb.taxid import TaxID
from operator import itemgetter
import sys

# create the top-level parser
parser = argparse.ArgumentParser()
parser.add_argument("--taxadb", help="Taxadb databse", type=Path)
parser.add_argument("--sgbtaxo", help="SGB taxonomy (GTDB format)", default=Path("mpa_vOct22_CHOCOPhlAnSGB_202212_SGB2GTDB.tsv.gz"),
                    type=Path)
parser.add_argument("--keepZeros", help="Do not remove SGB that have a null count in the final matrix",
                    action="store_false")
parser.add_argument('metaphlan_SBG_agg', type=Path, nargs='+')
parser.add_argument('output_matrix', type=Path, default="matrix.tsv", help="Combine all metaphlan SGB data into a contengency matrix")
parser.add_argument('output_taxo_table', type=Path, default="taxonomy.tsv", help="If sgbtaxo available, output the taxonomy of existing SGB")
args = parser.parse_args()
dataframes = [pd.read_csv(f, delimiter="\t") for f in args.metaphlan_SBG_agg]

# Now you can concatenate them into one DataFrame
df = dataframes[0]
for dataframe in dataframes[1:]:
    # print(dataframe)
    # dataframe = dataframe.set_index('SGB')
    # df = pd.merge(df, dataframe, left_index=True, right_index=True)
    df = pd.merge(df, dataframe, on='SGB')

df = df.set_index('SGB')
accession_db = AccessionID(dbtype="sqlite", dbname=args.taxadb)
tax_db = TaxID(dbtype="sqlite", dbname=args.taxadb)

# Remove rows with only zeros
if args.keepZeros:
    df = df[df.select_dtypes(include=[np.number]).sum(axis=1) >=10 ]
    df = df.loc[:, (df.sum(axis=0) >= 10)]
df.to_csv(args.output_matrix, sep="\t")
df["SGB"] = df.index
if args.sgbtaxo:
    taxo = pd.read_csv(args.sgbtaxo, compression="gzip", delimiter="\t", names=["SGB", "Annotation"])
    # print(taxo)
    taxo = taxo[taxo['SGB'].isin(df['SGB'])]
    # print(df.index)
    # taxo = taxo[taxo['SGB'].isin(df.index)]
    print(taxo)
    taxo_not_in_df = df[~df['SGB'].isin(taxo['SGB'])]
    # taxo_not_in_df = df[~df.index.isin(taxo['SGB'])]
    # accession=taxo_not_in_df['SGB'].values.tolist()
    taxids =taxo_not_in_df['SGB'].values.tolist()
    # chunks = [accession[i : i + 999] for i in range(0, len(accession), 999)]
    ncbi = {}
    # for accession_chunk in chunks:
        # taxids = accession_db.taxid(accession_chunk)
        # for tax in taxids:
    for tax in taxids:
        if tax.startswith("SGB"):
            continue
        lineage = tax_db.lineage_name(tax, reverse=True)
        if not lineage:
            continue
        if tax not in ncbi:
            if lineage[1] == "Eukaryota" and len(lineage)==17:
                ncbi[tax] = itemgetter(1,5,10,12,13,14,16)(lineage)
            elif lineage[1] == "Eukaryota" and len(lineage)==16:
                ncbi[tax] = itemgetter(1,5,9,11,12,13,15)(lineage)
            elif lineage[1] == "Eukaryota" and len(lineage)==15:
                ncbi[tax] = itemgetter(1,5,8,9,11,12,14)(lineage)
            elif lineage[1] == "Eukaryota" and len(lineage)==14:
                ncbi[tax] = itemgetter(1,5,8,9,11,12,13)(lineage)
            elif lineage[1] == "Eukaryota" and len(lineage)==13:
                ncbi[tax] = itemgetter(1,5,7,8,9,10,12)(lineage)
            # elif lineage[1] == "Eukaryota" and len(lineage)==16:
            #     ncbi[tax[1]] = itemgetter(1,5,7,8,9,10,12)(lineage)
            elif len(lineage)==12:
                ncbi[tax] = itemgetter(1,5,7,8,9,10,11)(lineage)
            elif len(lineage)==11:
                ncbi[tax] = itemgetter(1,4,5,7,8,9,10)(lineage)
            elif len(lineage)==10:
                ncbi[tax] = itemgetter(1,3,4,6,7,8,9)(lineage)
            elif len(lineage) == 9:
                ncbi[tax] = itemgetter(1,2,3,4,6,7,8)(lineage)
            elif len(lineage) == 8:
                ncbi[tax] = lineage[1:]
            elif len(lineage) > 0 and len(lineage) < 7:
                ncbi[tax] = lineage + ['']*(7-len(lineage))
            else:
                print(lineage)
                print(len(lineage))
                sys.exit("Lineage not handled")
    ncbi_df = pd.DataFrame(ncbi)
    ncbi_df_transposed = ncbi_df.transpose()
    ncbi_df_transposed.reset_index(level=0, inplace=True)
    ncbi_df_transposed = ncbi_df_transposed.astype(str)
    ncbi_df_transposed.columns = ['SGB', 'Kingdom', 'Phylum', 'Class', 'Order', 'Family', 'Genus', 'Species']
    ncbi_df_transposed['marker_SGB'] = ncbi_df_transposed['SGB'].astype(str) + '_' + ncbi_df_transposed['Species']
    taxo['Annotation'] = taxo['Annotation'].str.replace(r'\w__', '', regex=True)
    # print(ncbi_df_transposed)
    # Split 'Annotation' into multiple columns
    # print(taxo['Annotation'].str.split('|', expand=True))
    split_annotations = taxo['Annotation'].str.split(',').str[0].str.split('|', expand=True)
    # Select only the first 7 columns
    split_annotations = split_annotations.iloc[:, :7]
    # print(split_annotations)
    if split_annotations.shape[1] == 7:
        taxo[['Kingdom', 'Phylum', 'Class', 'Order', 'Family', 'Genus', 'Species']] =  split_annotations
        # Add 'marker_SGB' column
        taxo['marker_SGB'] = taxo['SGB'] + '_' + taxo['Annotation'].apply(lambda x: [i for i in x.split('|') if i][-1])
        del taxo['Annotation']
        # print(taxo)
        combined_df = pd.concat([taxo, ncbi_df_transposed], ignore_index=True)
        combined_df.to_csv(args.output_taxo_table, sep="\t", index=False)
