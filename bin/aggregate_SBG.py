#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html
import argparse
import pandas as pd
from pathlib import Path
# import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("metaphlan_file", type=Path)
parser.add_argument("metaphlan_len", type=Path)
parser.add_argument("sample_name", type=str)
parser.add_argument("output_file", type=Path)
args = parser.parse_args()

core_content = pd.read_csv(args.metaphlan_file, delimiter="\t", names=["gene", "count"], low_memory=False, comment='#')
# print(core_content)
metaphlan_len = pd.read_csv(args.metaphlan_len, delimiter="\t", compression="gzip", names=["gene", "len"])
# print(metaphlan_len)
core_content = core_content.merge(metaphlan_len[["gene", "len"]],on="gene", how="inner")
core_content["SGB"] = core_content['gene'].str.split('|').str[-1]
# Find rows where the character '|' is missing (should exclude regex here to avoid special character interpretation)
missing_pipe = core_content[~core_content['gene'].str.contains('\|', regex=True)]
# Split the 'gene_info' column in 'missing_pipe' by '__' and get the first part, which should be 'SGB'
SGB_from_missing_pipe = missing_pipe['gene'].str.split('__', expand=True)[0]
# Assign the SGB values back to the 'SGB' column in 'df' where they were missing
core_content.loc[missing_pipe.index, 'SGB'] = SGB_from_missing_pipe
# core_content.to_csv("core_content.tsv", sep="\t")
# Select lines that do not start with 'SGB'
core_content["NCBI"] = core_content.loc[~core_content['SGB'].str.startswith('SGB'), 'SGB']
# core_content['extracted_part'] = core_content['NCBI'].str.split('-').str[1].str.split('__').str[0].str.split('.').str[0]
core_content['extracted_part'] = core_content['NCBI'].str.split('_').str[0]
# Where extracted_part is not null, replace the corresponding SGB values
core_content.loc[~core_content['extracted_part'].isnull(), 'SGB'] = core_content.loc[~core_content['extracted_part'].isnull(), 'extracted_part']
core_content['len'] = pd.to_numeric(core_content['len'])
core_content['count'] = pd.to_numeric(core_content['count'])
core_content[args.sample_name] = core_content["count"]/core_content["len"]*10000
del core_content["len"]
del core_content["count"]
del core_content["gene"]
del core_content["NCBI"]
del core_content["extracted_part"]
# Sort the values according to 'SGB' and '85F-S'
# core_content = core_content.sort_values(by=['SGB', args.sample_name], ascending=False)
# core_content.to_csv("core_content.tsv", sep="\t")

# res = core_content.groupby("SGB")[args.sample_name].mean()
# result = core_content.groupby('SGB').apply(lambda x: print(x))
# res = core_content.groupby('SGB').apply(lambda x: x.nlargest(int(len(x)*0.33), args.sample_name)[args.sample_name]).mean(level=0)
# res = core_content.groupby('SGB').apply(lambda x: x.nlargest(int(len(x)*0.33), args.sample_name)).mean()
# res = core_content.groupby('SGB')[args.sample_name].agg(lambda x: x.quantile(0.67)).mean().reset_index(name='quantile_0.67')

# rows_num = core_content.groupby('SGB').size().apply(lambda x: int(x * 0.33))
# print(rows_num)
# top_values_list = []
# for group, num_rows in rows_num.items():
#     print(group)
#     top_values = core_content.loc[core_content['SGB'] == group].nlargest(num_rows, args.sample_name)
#     top_values_list.append(top_values)
# top_df = pd.concat(top_values_list, ignore_index=True)
# res = top_df.groupby('SGB').mean()

def top_n_rows(df, sample_name):
    n = int(df.shape[0] * 0.33)
    return df.nlargest(n, sample_name)

top_df = core_content.groupby('SGB', group_keys=False).apply(top_n_rows, sample_name=args.sample_name)
res = top_df.groupby('SGB').mean()
#res = core_content.groupby("SGB").agg("mean")
# res = res.round(0).astype(int)
#print(res)
res.to_csv(args.output_file, sep="\t")
