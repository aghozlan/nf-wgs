import pandas as pd
import numpy as np

# Create a DataFrame from the sample data
df = pd.read_csv("mpa_vJun23_CHOCOPhlAnSGB_202307_SGB_len.txt.gz", sep='\t', header=None, names=['gene_info', 'value'])

# Split the gene_info into gene_name, position, SGB
df[['gene_name', 'position', 'SGB']] = df['gene_info'].str.split('|', expand=True)

# Find rows where the character '|' is missing (should exclude regex here to avoid special character interpretation)
missing_pipe = df[~df['gene_info'].str.contains('\|', regex=True)]

# Split the 'gene_info' column in 'missing_pipe' by '__' and get the first part, which should be 'SGB'
SGB_from_missing_pipe = missing_pipe['gene_info'].str.split('__', expand=True)[0]

# Assign the SGB values back to the 'SGB' column in 'df' where they were missing
df.loc[missing_pipe.index, 'SGB'] = SGB_from_missing_pipe
#df[['position', 'SGB']] = df['position'].str.split('__', expand=True)
print(df['SGB'])
# Strip any accidental leading underscores or spaces from SGB column
#df['SGB'] = df['SGB'].str.strip('_')

# Count unique gene_name per SGB
gene_counts = df.groupby('SGB')['gene_name'].nunique()

# Count unique SGB numbers
unique_SGB_count = df['SGB'].nunique()
print(df['SGB'].unique())

# Calculate the average number of gene_name per SGB
average_genes_per_SGB = gene_counts.mean()
min_genes_per_SGB = gene_counts.min()
max_genes_per_SGB = gene_counts.max()
median_genes_per_SGB = gene_counts.median()

sgb_less_than_100 = gene_counts[gene_counts < 50].count()
print(f"Number of SGBs with less than 100 unique genes: {sgb_less_than_100}")
print(f"Number of unique SGBs: {unique_SGB_count}")
print(f"Average number of gene_names per SGB: {average_genes_per_SGB}")
print(f"Minimum number of gene_names per SGB: {min_genes_per_SGB}")
print(f"Maximum number of gene_names per SGB: {max_genes_per_SGB}")
print(f"Median number of gene_names per SGB: {median_genes_per_SGB}")
gene_counts.to_csv("marker_SGB.tsv",sep="\t")
