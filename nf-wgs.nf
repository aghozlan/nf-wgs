#!/usr/bin/env nextflow

workflow.onComplete = {
    // any workflow property can be used here
    println "Pipeline complete"
    println "Command line: $workflow.commandLine"
}


workflow.onError = {
    println "Oops .. something went wrong"
}

params.help=false

def usage() {
    println("nf-wgs.nf --in <reads_dir> --out <output_dir> --cpus <nb_cpus> -w <temp_work_dir>")
}

if(params.help){
    usage()
    exit(1)
}

params.catalogue = false
params.in="$baseDir/test/"
params.out = "$baseDir/result/"
params.alienseq = "$baseDir/databases/alienTrimmerPF8contaminants.fasta"
params.minimum_read_len = 35
params.sylphdb = "$baseDir/databases/v0.3-c200-gtdb-r214.syldb"
params.sylphtaxo = "$baseDir/databases/gtdb_r214_metadata.tsv.gz"
params.human_genome = "/local/databases/index/bowtie/hg38/hg38.fa"
params.krakendb = "/pasteur/zeus/projets/p01/BioIT/19505_Lazarini/kraken_standard/"
params.chocophlan_len = "$baseDir/databases/mpa_vJun23_CHOCOPhlAnSGB_202403_SGB_len.txt.gz"
params.chocophlan_gtdb = "$baseDir/databases/mpa_vJun23_CHOCOPhlAnSGB_202403_species.txt.gz"
params.chocophlan_db = "/pasteur/zeus/projets/p01/BioIT/amine/bich_tram/mitica_res/metaphlan_databases"
params.chocophlan_index = "mpa_vJun23_CHOCOPhlAnSGB_202403"
params.chocophlan_index_humann = "mpa_vOct22_CHOCOPhlAnSGB_202403"
params.cpus =  4
params.cleaned_reads = "${params.out}/cleaned_reads"
params.min_read_len_bracken = 80
params.min_read_nb_bracken = 10
params.taxadb = "/local/databases/rel/taxadb/current/db/taxadb_full.sqlite"
params.rvdb = "/pasteur/zeus/projets/p01/BioIT/19505_Lazarini/C-RVDBvCurrent"
params.mgv = "/pasteur/zeus/projets/p01/BioIT/19505_Lazarini/mgv_votu_representatives.fna"
params.mgv_contig_info = "/pasteur/zeus/projets/p01/BioIT/19505_Lazarini/mgv_contig_info.tsv"
params.filter_seq = "/pasteur/zeus/projets/p01/BioIT/19505_Lazarini/clusterRes_rep_seq.fasta"
params.depth = 100
params.tmp = "/local/scratch/tmp"
myDir = file(params.out)
myDir.mkdirs()

cleanDir = file("${params.cleaned_reads}")
cleanDir.mkdirs()

process alientrimmer {
    tag { reads_id }
    module "graalvm/ce-java8-20.0.0:AlienTrimmer/2.1"

    input:
    tuple val(reads_id), path(reads)

    output:
    tuple val(reads_id), path("*_trim.1.fastq"), path("*_trim.2.fastq"), emit: trimmed_seqs

    """
    AlienTrimmer -1 ${reads[0]} -2 ${reads[1]} -q 20  -p 80 -k 10 -m 5 -l ${params.minimum_read_len} -o ${reads_id}_trim -a ${params.alienseq}
    """
}

process bowtie2 {
    tag { reads_id }
    module "bowtie2/2.5.1"
    cpus params.cpus
    memory "30GB"
    beforeScript "mkdir no_human"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    tuple val(reads_id), path("no_human/*.1"), path("no_human/*.2"), emit: filtered_seqs

    """
    bowtie2 -p ${params.cpus} --very-fast -x ${params.human_genome} -1 ${forward} -2 ${reverse} -S /dev/null  --un-conc no_human/
    """
}


process kraken {
    // publishDir "$myDir/kraken_profiles", mode: 'copy', pattern: '*.kraken'
    // publishDir "$myDir/kraken_report", mode: 'copy', pattern: '*.report'
    tag { reads_id }
    cpus params.cpus
    memory "50G"
    module "kraken/2.1.3"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    tuple val(reads_id), path("${reads_id}.kraken"), path("${reads_id}.report"), path(forward), path(reverse), emit: kraken_seqs

    """
    kraken2 --threads ${params.cpus} --db ${params.krakendb}  --report ${reads_id}.report  --output ${reads_id}.kraken --paired ${forward} ${reverse}
    """
}

process filter_reads {
    tag { reads_id }
    cpus params.cpus
    memory "50G"
    module "Python/3.11.5"


    input:
    tuple val(reads_id), path(kraken), path(report), path(forward), path(reverse)

    output:
    tuple val(reads_id), path(report), path("${reads_id}_R1.fastq"), path("${reads_id}_R2.fastq"), emit: filtered_seqs

    """
    extract_kraken_reads.py --exclude --fastq-output -r ${report}  -k ${kraken} -t 9606 2886930 -s ${forward} -s2 ${reverse} -o ${reads_id}_R1.fastq -o2 ${reads_id}_R2.fastq
    """
}

process validate_pair {
    tag { reads_id }
    conda "/pasteur/zeus/projets/p01/BioIT/amine/miniconda3/envs/filterfastq"
    memory "20G"
    cpus params.cpus
    afterScript "mv ${reads_id}_R1.fastq.paired.fq ${reads_id}_R1.fastq; mv ${reads_id}_R2.fastq.paired.fq ${reads_id}_R2.fastq; pigz -p ${params.cpus} *.fastq"

    input:
    tuple val(reads_id), path(report), path(forward), path(reverse)

    output:
    tuple val(reads_id), path(report), file("*_R1.fastq.gz"), file("*_R2.fastq.gz"), emit: valid_seqs

    """
    fastq_pair ${forward} ${reverse}
    """
}

process filter_control {
    tag { reads_id }
    publishDir "$cleanDir/", mode: 'copy', pattern: '*_R*.fastq.gz'
    memory "50G"
    cpus params.cpus
    beforeScript "mkdir filter"
    afterScript "mv filter/un-conc-mate.1 filter/${reads_id}_R1.fastq.gz; mv filter/un-conc-mate.2 filter/${reads_id}_R2.fastq.gz"
    module "bowtie2"

    input:
    tuple val(reads_id), path(report), path(forward), path(reverse)

    output:
    tuple val(reads_id), path(report), file("filter/*_R1.fastq.gz"), file("filter/*_R2.fastq.gz"), emit: filter_seqs

    """
    bowtie2 -p ${params.cpus} --very-fast -x ${params.filter_seq} -1 ${forward} -2 ${reverse} -S /dev/null  --un-conc-gz filter/ > align_info
    """
}

process khmer {
    cpus params.cpus
    module "khmer"
    afterScript "pigz -p ${params.cpus} *_filt_R{1,2}.fastq"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    tuple val(reads_id), file("*_filt_R1.fastq.gz"), file("*_filt_R2.fastq.gz"), emit: filter_seqs

    """
    interleave-reads.py ${forward} ${reverse} --output interleaved.pe
    normalize-by-median.py -p -k 20 -C 20 -N 4 -x 3e9 --savegraph graph.ct  interleaved.pe --output output.pe.keep
    filter-abund.py -V graph.ct output.pe.keep --output output.pe.filter -T ${params.cpus}
    extract-paired-reads.py output.pe.filter --output-paired output.dn.pe  --output-single output.dn.se
    split-paired-reads.py output.dn.pe -1 ${reads_id}_filt_R1.fastq -2 ${reads_id}_filt_R2.fastq
    """
}

process megahit {
    publishDir "$myDir/assembly", mode: 'copy'
    cpus params.cpus
    module "SPAdes/4.0.0:Python/3.11.5"
    beforeScript "mkdir assembly"
    afterScript "mv assembly/${reads_id}_megahit.contigs.fa assembly/${reads_id}_megahit.fasta;sed -i \"s/ /_/g\" assembly/${reads_id}_megahit.fasta"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    tuple val(reads_id), path("assembly/${reads_id}_spades.fasta")

    """
    spades.py --meta --only-assembler -1 ${forward} -2 ${reverse} -t ${params.cpus} -o assembly/ -m 500
    rename_fasta.py -i assembly/contigs.fasta -s ${reads_id} -o assembly/${reads_id}_spades.fasta
    """
}

process prodigal {
    module "prodigal"

    input:
    tuple  val(reads_id), path(contigs)

    output:
    tuple  val(reads_id), path("${reads_id}_predicted_cds.fna")

    """
    prodigal -m -p meta -d ${reads_id}_predicted_cds.fna -c -i ${contigs}
    """
}

process mmseqs2 {
    publishDir "$myDir/catalogue", mode: 'copy'
    module "MMseqs2"
    beforeScript "mkdir catalogue"
    afterScript "rm -rf /local/scratch/tmp_dir"

    input:
    path(gene)

    output:
    path("catalogue")

    """
    mmseqs easy-linclust ${gene} catalogue /local/scratch/tmp_dir --min-seq-id 0.95 -c 0.9 --cov-mode 0
    """
}

process sylph {
    publishDir "$myDir/sylph_profile", mode: 'copy'
    cpus params.cpus
    module "Python/3.11.5"

    input:
    path(reads)

    output:
    tuple path("profiling.tsv"), path("reads.txt"), emit: output

    """
    $baseDir/bin/sylph profile  ${params.sylphdb} -t ${params.cpus} -1 *_R1.fastq.gz -2 *_R2.fastq.gz > profiling.tsv
    r1_files=\$(ls *_R1.fastq.gz)
    countFastq.py \$r1_files reads.txt
    """
}

process sylph_post {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"

    input:
    tuple path(profiling), path(reads)

    output:
    path("counts_sylph.tsv"),  optional: true
    path("taxonomy_sylph.tsv"),  optional: true

    """
    sylph_post.py ${profiling} ${reads} ${params.sylphtaxo} counts_sylph.tsv taxonomy_sylph.tsv
    """
}

process bracken {
    tag { reads_id }
    // publishDir "$myDir/bracken_report", mode: 'copy'
    module "kraken/2.1.3:Bracken/2.6.2"

    input:
    tuple val(reads_id), path(report)

    output:
    path("${reads_id}.bracken_report")
    path("${reads_id}.bracken"), emit: bracken_annotation

    """
    bracken -d ${params.krakendb} -i ${report} -r ${params.min_read_len_bracken} -w ${reads_id}.bracken_report -o ${reads_id}.bracken -t ${params.min_read_nb_bracken}
    """
}

process bracken_post {
    publishDir "$myDir/", mode: 'copy'

    input:
    path(bracken_results)

    output:
    path("counts_bracken.tsv"),  optional: true
    path("taxonomy_bracken.tsv"),  optional: true

    """
    bracken_post.py --taxadb ${params.taxadb}  ${bracken_results} counts_bracken.tsv taxonomy_bracken.tsv
    """
}

process metaphlan {
    tag { reads_id }
    // publishDir "$myDir/metaphlan_bowtie2", mode: 'copy', pattern: '*.bowtie2.bz2'
    // publishDir "$myDir/metaphlan_profiles", mode: 'copy', pattern: '*_profile.txt'
    // publishDir "$myDir/virus_report", mode: 'copy', pattern: '*_vsc.txt'
    memory "30G"
    cpus params.cpus
    module "Python/3.11.5:samtools"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    //path("${reads_id}_vsc.txt "),
    path("${reads_id}_metaphlan_profile.tsv"), emit: metaphlan_annotation
    path("${reads_id}_reads.txt"),   emit: reads
    tuple val(reads_id), path("${reads_id}_vsc.txt"), emit: vsc

    """
    metaphlan --profile_vsc --vsc_out ${reads_id}_vsc.txt --tax_lev t -x ${params.chocophlan_index} --bowtie2db ${params.chocophlan_db} --offline --input_type fastq --nproc ${params.cpus} --bowtie2out ${reads_id}.bowtie2.bz2  ${forward},${reverse} ${reads_id}_metaphlan_profile.tsv
    lines=\$(zcat ${forward} | wc -l | bc -l)
    reads=\$(echo "\$lines / 4" | bc)
    echo -e "${reads_id}\t\$reads" > ${reads_id}_reads.txt
    """
}

process merge_vsc {
    tag { sample_name }
    module "Python/3.11.5"

    input:
    tuple val(sample_name), path(profile_vsc)

    output:
    path("${sample_name}_merged.tsv"), emit: merged

    """
    #!/usr/bin/env python3
    import pandas as pd
    df = pd.read_csv("${profile_vsc}", sep="\\t", comment='#')
    # Add Sample_Name as the first column
    df.insert(0, 'Sample_Name', "${sample_name}")
    df.to_csv("${sample_name}_merged.tsv", index=False, sep="\\t")
    """
}

process merge_metaphlan {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"

    input:
    path(profile)
    path(reads)

    output:
    path("counts_metaphlan.tsv"),  optional: true
    path("taxonomy_metaphlan.tsv"),  optional: true

    """
    cat *_reads.txt > reads_depth.txt
    merge_metaphlan_tables.py *_profile.tsv > merged_profile.txt
    metaphlan_post.py ${params.chocophlan_gtdb} merged_profile.txt reads_depth.txt counts_metaphlan.tsv taxonomy_metaphlan.tsv
    """
}
process metaphlan_marker_counts {
    tag { reads_id }
    // publishDir "$myDir/metaphlan_profiles", mode: 'copy', pattern: '*.bowtie2.bz2'
    // publishDir "$myDir/metaphlan_profiles_marker", mode: 'copy', pattern: '*_profile.txt'

    memory "30G"
    cpus params.cpus
    module "Python/3.11.5"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    //path("${reads_id}_vsc.txt "),
    tuple val(reads_id), path("${reads_id}_profile.txt"),  emit: annotation
    //--profile_vsc --vsc_out ${reads_id}_vsc.txt

    """
    metaphlan -x ${params.chocophlan_index} --bowtie2db ${params.chocophlan_db} -t marker_counts --offline --input_type fastq --nproc ${params.cpus} --bowtie2out ${reads_id}.bowtie2.bz2  ${forward},${reverse} ${reads_id}_profile.txt
    """
}

process aggregate_SBG {
    tag { reads_id }
    module "Python/3.11.5"
    memory "10G"

    input:
    tuple val(reads_id), path(profile) //, path(virus_profiles)

    output:
    path("${reads_id}_aggregated.tsv")

    """
    aggregate_SBG.py ${profile}  ${params.chocophlan_len}  ${reads_id} ${reads_id}_aggregated.tsv
    """
}

process build_matrix {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"

    input:
    path(aggregated) //.toList()

    output:
    path("counts_metaphlan_marker_gene.tsv"),  optional: true
    path("taxonomy_metaphlan_marker_gene.tsv"),  optional: true

    """
    build_matrix.py --taxadb ${params.taxadb} --sgbtaxo ${params.chocophlan_gtdb}  ${aggregated}   counts_metaphlan_marker_gene.tsv taxonomy_metaphlan_marker_gene.tsv
    """
}

process rvdb {
    tag { reads_id }
    module "bowtie2:samtools"
    cpus params.cpus
    memory "20G"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    path("${reads_id}.tsv"),  emit: counts
    path("${reads_id}_cov.tsv"), emit: covs

    """
    bowtie2 -p ${params.cpus} -x ${params.rvdb} -1 ${forward} -2 ${reverse} | samtools view -@ ${params.cpus} -bh - | samtools sort -@ ${params.cpus} -o ${reads_id}.bam
    # Create an index for the BAM file
    samtools index -@ ${params.cpus} ${reads_id}.bam ${reads_id}.bam.bai
    samtools idxstats ${reads_id}.bam > ${reads_id}.tsv
    samtools coverage --reference  ${params.rvdb} -d ${params.depth} -o ${reads_id}_cov.tsv  ${reads_id}.bam
    """
}

process merge_rvdb {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"
    memory "100G"

    input:
    path(counts)
    path(coverage)

    output:
    path("counts_rvdb.tsv"),  optional: true
    path("taxonomy_rvdb.tsv"),  optional: true

    """
    rvdb_matrix.py --taxadb ${params.taxadb} --cov ${coverage} --counts ${counts} --om counts_rvdb.tsv
    """
}

process humann {
    tag { reads_id }
    memory "60G"
    cpus params.cpus
    module "Python/3.11.5:diamond"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    path("${reads_id}_humann/*.tsv"), emit: humann_annotation,  optional: true

    """
    cat ${forward} ${reverse} > ${reads_id}.fastq.gz
    humann -i ${reads_id}.fastq.gz --metaphlan-options \'-x ${params.chocophlan_index_humann} -t rel_ab_w_read_stats --bowtie2db ${params.chocophlan_db} --offline --tmp_dir ${params.tmp}\' --count-normalization Counts --output ${reads_id}_humann --threads ${params.cpus} --remove-temp-output
    exit_status=\$?      # Capture the exit status of the previous command
    [ \$exit_status -eq 140 ] && exit 0 || exit \$exit_status
    """
}

process merge_humann {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"
    memory "20G"

    input:
    path(humann_annotations)

    output:
    path("humann_{pathabundance,reactions}.tsv"), emit: merge_data
    path("humann_genefamilies.tsv")

    """
    humann_join_tables -i ./ -o humann_pathabundance.tsv --file_name pathabundance
    humann_join_tables -i ./ -o humann_genefamilies.tsv --file_name genefamilies
    humann_join_tables -i ./ -o humann_reactions.tsv --file_name reactions
    """
}

process filter_humann {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"
    memory "20G"

    input:
    path(humann_res)

    output:
    path("*_filter.tsv")

    """
    #!/usr/bin/env python3
    from pathlib import Path
    import pandas as pd
    sample = Path("${humann_res}")
    type_sample = sample.stem.split("_")[-1]
    df = pd.read_csv(sample, sep="\t", header=0, engine="c")
    naughty_list = ['UNMAPPED', 'UNINTEGRATED', 'UNINTEGRATED|unclassified', 'READS_UNMAPPED']
    column_to_remove = 'Counts'
    if column_to_remove in df.columns:
        df.drop(column_to_remove, axis=1, inplace=True)
    df.columns = ["_".join(col.split("_")[:-1]) if col.endswith("_Abundance")  else col for col in df.columns]
    df.columns.values[0] = 'Name'
    df = df[~df['Name'].str.startswith('UNINTEGRATED')]
    df = df[~df['Name'].str.startswith('UNGROUPED')]
    df = df[~df['Name'].isin(naughty_list)]
    df = df.loc[:, (df != 0).any(axis=0)]
    row_sums = df.drop('Name', axis=1).sum(axis=1)
    df = df[row_sums != 0]
    df.to_csv(f"{type_sample}_filter.tsv", index=False, sep="\t")
    """
}

process mgv {
    tag { reads_id }
    module "bowtie2:samtools"
    cpus params.cpus
    memory "20G"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    path("${reads_id}.tsv"),  emit: counts
    path("${reads_id}_cov.tsv"), emit: covs

    """
    bowtie2 -p ${params.cpus} -x ${params.mgv} -1 ${forward} -2 ${reverse} | samtools view -@ ${params.cpus} -bh - | samtools sort -@ ${params.cpus} -o ${reads_id}.bam
    # Create an index for the BAM file
    samtools index -@ ${params.cpus} ${reads_id}.bam ${reads_id}.bam.bai
    samtools idxstats ${reads_id}.bam > ${reads_id}.tsv
    samtools coverage --reference  ${params.mgv} -d ${params.depth} -o ${reads_id}_cov.tsv  ${reads_id}.bam
    """
}

process merge_mgv {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"
    memory "100G"

    input:
    path(counts)
    path(coverage)

    output:
    path("counts_mgv.tsv"),  optional: true

    """
    mgv_matrix.py  --mgv_contig_info ${params.mgv_contig_info} --cov ${coverage} --counts ${counts} --om counts_mgv.tsv
    """
}

workflow {
    readChannel = Channel.fromFilePairs("${params.in}/*_{1,2}.{fastq,fastq.gz,fq,fq.gz}")
                    .ifEmpty { exit 1, "Cannot find any reads matching: ${params.in}" }
    alientrimmer(readChannel) | bowtie2 | kraken | filter_reads | validate_pair | filter_control
    filter_control.out.filter_seqs
        .map { id, report, r1, r2 -> [ [id, report], [id, r1, r2] ] }
        .multiMap { it ->
            sylph: it[1]
            metaphlan: it[1]
            bracken: it[0]
            rvdb: it[1]
            mgv: it[1]
            humann: it[1]
        }
        .set { reads_branch }

    bracken(reads_branch.bracken)
    bracken_files = bracken.out.bracken_annotation.collect(flat: false)
    // Pass the list of files to another process
    bracken_post(bracken_files)
    metaphlan_marker_counts(reads_branch.metaphlan) | aggregate_SBG | collect(flat: false) | build_matrix
    metaphlan(reads_branch.metaphlan)
    merge_vsc(metaphlan.out.vsc)
    merge_vsc.out.merged.collectFile(keepHeader: true, skip: 1, name:'result', sort: true) | subscribe { it.copyTo("${myDir}/vsc_out.tsv") }
    metaphlan_vsc = metaphlan.out.vsc.collect(flat: false)
    metaphlan_annotations = metaphlan.out.metaphlan_annotation.collect(flat: false)
    metaphlan_reads = metaphlan.out.reads.collect(flat: false)
    merge_metaphlan(metaphlan_annotations, metaphlan_reads)
    reads_branch.sylph
        .map { it -> [it[1], it[2]] }
        .flatten()
        .toList()
        .set { sylph_input }
    sylph(sylph_input)|sylph_post
    rvdb(reads_branch.rvdb)
    counts_rvdb = rvdb.out.counts.collect(flat: false)
    covs_rvdb = rvdb.out.covs.collect(flat: false)
    merge_rvdb(counts_rvdb, covs_rvdb)
    mgv(reads_branch.mgv)
    counts_mgv = mgv.out.counts.collect(flat: false)
    covs_mgv = mgv.out.covs.collect(flat: false)
    merge_mgv(counts_mgv, covs_mgv)
    humann(reads_branch.humann)
    humann_annotations = humann.out.humann_annotation.collect(flat: true)
    merge_humann(humann_annotations)
    merged_annotations = merge_humann.out.merge_data.collect()
    filter_humann(merged_annotations.flatten())
}

