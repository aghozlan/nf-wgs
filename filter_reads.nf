#!/usr/bin/env nextflow

workflow.onComplete = {
    // any workflow property can be used here
    println "Pipeline complete"
    println "Command line: $workflow.commandLine"
}


workflow.onError = {
    println "Oops .. something went wrong"
}

params.help=false

def usage() {
    println("nf-wgs.nf --in <reads_dir> --out <output_dir> --cpus <nb_cpus> -w <temp_work_dir>")
}

if(params.help){
    usage()
    exit(1)
}

params.in="$baseDir/test/"
params.out = "$baseDir/result/"
params.cpus=4


myDir = file(params.out)
myDir.mkdirs()

process validate_pair {
    tag { reads_id }
    conda "/pasteur/zeus/projets/p01/BioIT/amine/miniconda3/envs/filterfastq"
    memory "20G"

    input:
    tuple val(reads_id), path(reads)

    output:
    tuple val(reads_id), file("*_R1.paired.fq"), file("*_R2.paired.fq"), emit: valid_seqs

    """
    gunzip -c ${reads[0]} > ${reads_id}_R1
    gunzip -c ${reads[1]} > ${reads_id}_R2
    fastq_pair ${reads_id}_R1 ${reads_id}_R2
    """
}

process khmer {
    tag { reads_id }
    cpus params.cpus
    memory "50G"
    module "khmer/2.1.2"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    tuple val(reads_id), path("*_filt_R1.fastq"), path("*_filt_R2.fastq"), emit: khmer

    """
    interleave-reads.py ${forward} ${reverse} --output interleaved.pe
    normalize-by-median.py -p -k 20 -C 20 -N 4 -x 3e9 --savegraph graph.ct  interleaved.pe --output output.pe.keep
    filter-abund.py -V graph.ct output.pe.keep --output output.pe.filter -T ${params.cpus}
    extract-paired-reads.py output.pe.filter --output-paired output.dn.pe  --output-single output.dn.se
    split-paired-reads.py output.dn.pe -1 ${reads_id}_filt_R1.fastq -2 ${reads_id}_filt_R2.fastq
    """
}

process megahit {
    tag { reads_id }
    cpus params.cpus
    memory "120G"
    module "megahit/1.2.9"
    afterScript "mv assembly/${reads_id}_megahit.contigs.fa assembly/${reads_id}_megahit.fasta; sed -i \"s/ /_/g\" assembly/${reads_id}_megahit.fasta; pigz -p ${params.cpus} assembly/${reads_id}_megahit.fasta"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    path("assembly/${reads_id}_megahit.fasta.gz"), emit: assembly

    """
    megahit  -t ${params.cpus} -1 ${forward} -2 ${reverse} -o assembly --out-prefix ${reads_id}_megahit
    """
}

workflow {
    readChannel = Channel.fromFilePairs("${params.in}/T*_R{1,2}.{fastq,fastq.gz,fq,fq.gz}")
                    .ifEmpty { exit 1, "Cannot find any reads matching: ${params.in}" }
    validate_pair(readChannel)| khmer | megahit
    megahit.out.assembly.collectFile(name: 'concatenated_conta.fasta.gz', newLine: true, storeDir: myDir)
}
