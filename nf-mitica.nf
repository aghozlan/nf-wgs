#!/usr/bin/env nextflow

workflow.onComplete = {
    // any workflow property can be used here
    println "Pipeline complete"
    println "Command line: $workflow.commandLine"
}


workflow.onError = {
    println "Oops .. something went wrong"
}

params.help=false

def usage() {
    println("nf-wgs.nf --in <reads_dir> --out <output_dir> --cpus <nb_cpus> -w <temp_work_dir>")
}

if(params.help){
    usage()
    exit(1)
}

params.catalogue = false
params.in="$baseDir/test/"
params.out = "$baseDir/result/"
params.alienseq = "$baseDir/databases/alienTrimmerPF8contaminants.fasta"
params.minimum_read_len = 35
params.sylphdb = "$baseDir/databases/v0.3-c200-gtdb-r214.syldb"
params.sylphtaxo = "$baseDir/databases/gtdb_r214_metadata.tsv.gz"
params.human_genome = "/local/databases/index/bowtie/hg38/hg38.fa"
params.krakendb = "/pasteur/zeus/projets/p01/BioIT/19505_Lazarini/kraken_standard/"
params.chocophlan_len = "$baseDir/databases/mpa_vJun23_CHOCOPhlAnSGB_202403_SGB_len.txt.gz"
params.chocophlan_gtdb = "$baseDir/databases/mpa_vJun23_CHOCOPhlAnSGB_202403_species.txt.gz"
params.chocophlan_db = "/pasteur/zeus/projets/p01/BioIT/amine/bich_tram/mitica_res/metaphlan_databases"
params.chocophlan_index = "mpa_vJun23_CHOCOPhlAnSGB_202403"
params.chocophlan_pkl = "${params.chocophlan_db}/${params.chocophlan_index}.pkl"
params.chocophlan_index_humann = "mpa_vOct22_CHOCOPhlAnSGB_202403"
params.tmp = "/local/scratch/tmp"
params.cpus =  4
params.cleaned_reads = "${params.out}/cleaned_reads"
params.min_read_len_bracken = 80
params.min_read_nb_bracken = 10
params.taxadb = "/local/databases/rel/taxadb/current/db/taxadb_full.sqlite"
params.rvdb = "/pasteur/zeus/projets/p01/BioIT/19505_Lazarini/C-RVDBvCurrent"
params.mgv = "/pasteur/zeus/projets/p01/BioIT/19505_Lazarini/mgv_votu_representatives.fna"
params.mgv_contig_info = "/pasteur/zeus/projets/p01/BioIT/19505_Lazarini/mgv_contig_info.tsv"
params.depth = 100
myDir = file(params.out)
myDir.mkdirs()

cleanDir = file("${params.cleaned_reads}")
cleanDir.mkdirs()

process validate_pair {
    tag { reads_id }
    conda "/pasteur/zeus/projets/p01/BioIT/amine/miniconda3/envs/filterfastq"
    memory "20G"
    cpus params.cpus
    afterScript "mv ${reads_id}_R1.paired.fq ${reads_id}_valid_R1.fastq; mv ${reads_id}_R2.paired.fq ${reads_id}_valid_R2.fastq;pigz -p ${params.cpus} *.fastq"

    input:
    tuple val(reads_id), path(reads)

    output:
    tuple val(reads_id), file("*_R1.fastq.gz"), file("*_R2.fastq.gz"), emit: reads

    """
    gunzip -c ${reads[0]} > ${reads_id}_R1
    gunzip -c ${reads[1]} > ${reads_id}_R2
    fastq_pair ${reads_id}_R1 ${reads_id}_R2
    """
}

process alignchocophlan {
    cpus params.cpus
    module "bowtie2:samtools"
    memory "20G"
    afterScript "grep -oP \"(\\d+) reads;\" alignsummary | grep -oP \"\\d+\" > nreads.txt"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    tuple val(reads_id), path("nreads.txt"), path("${reads_id}.sam"), emit: sam

    """
    bowtie2 -p ${params.cpus} --no-unal --very-sensitive -S ${reads_id}.sam -x ${params.chocophlan_db}/${params.chocophlan_index} -U ${forward},${reverse} 2> alignsummary
    """
}

process metaphlan {
    tag { reads_id }
    // publishDir "$myDir/metaphlan_bowtie2", mode: 'copy', pattern: '*.bowtie2.bz2'
    // publishDir "$myDir/metaphlan_profiles", mode: 'copy', pattern: '*_profile.txt'
    // publishDir "$myDir/virus_report", mode: 'copy', pattern: '*_vsc.txt'
    memory "10G"
    module "Python/3.11.5:samtools"

    input:
    tuple val(reads_id), path(nreads), path(sam)

    output:
    path("${reads_id}_metaphlan_profile.tsv"), emit: relative_abundance
    path("${reads_id}_reads.txt"), emit: counts
    tuple val(reads_id), path("${reads_id}_marker_count_profile.tsv"),  emit: marker_counts

    """
    nreads_val=\$(cat ${nreads})
    echo -e "${reads_id}\\t\${nreads_val}" > "${reads_id}_reads.txt"
    metaphlan --tax_lev t --tmp_dir ${params.tmp} --input_type sam  ${sam} ${reads_id}_metaphlan_profile.tsv -x ${params.chocophlan_index} --offline --bowtie2db ${params.chocophlan_db} --nreads \${nreads_val}
    metaphlan -t marker_counts --input_type sam  ${sam} ${reads_id}_marker_count_profile.tsv -x ${params.chocophlan_index} --offline --bowtie2db ${params.chocophlan_db} --nreads \${nreads_val}
    """
}

process compressSAM {
    tag "$reads_id" // Using the tag for easy identification of jobs

    input:
    tuple val(reads_id), path(nreads), path(sam_file)

    output:
    path("${sam_file}.bz2"), emit: bz2sam // Emitting the name of the compressed file

    script:
    """
    bzip2 -c ${sam_file} > ${sam_file}.bz2
    """
}

process sample2markers {
    memory "30G"
    module "Python/3.11.5"
    beforeScript "mkdir consensus"

    input:
    path(sam)

    output:
    path("consensus/*"), emit: markers

    """
    sample2markers.py -i ${sam} -o consensus -n ${params.cpus} -d ${params.chocophlan_pkl}
    """
}

process extract_markers {
    module "Python/3.11.5"
    beforeScript "mkdir db_markers"
    memory "20G"

    input:
    path(taxo_matrix)

    output:
    path("db_markers/*"), emit: markers_fna

    """
    marker=\$(cut -f1 ${taxo_matrix} | tail -n+2 | tr '\\n' ' ')
    extract_markers.py -c \$(for val in \${marker}; do echo "t__\$val"; done) -b ${params.chocophlan_db}/  -o db_markers/
    """
}

process strainphlan  {
    publishDir "$myDir", mode: 'copy'
    cpus params.cpus

    input:
    path(markers)
    path(markers_fna)

    output:
    path("strain_follow_up/*")

    """
    strainphlan -s ${markers} -m ${markers_fna} -n ${params.cpus} --mutation_rates -o strain_follow_up
    """
}

process merge_metaphlan {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"

    input:
    path(profile)
    path(reads)

    output:
    path("counts_metaphlan.tsv"),  emit: relative_abundance_counts
    path("taxonomy_metaphlan.tsv"),  emit: relative_abundance_taxo

    """
    cat *_reads.txt > reads_depth.txt
    merge_metaphlan_tables.py *_profile.tsv > merged_profile.txt
    metaphlan_post.py ${params.chocophlan_gtdb} merged_profile.txt reads_depth.txt counts_metaphlan.tsv taxonomy_metaphlan.tsv
    """
}

process aggregate_SBG {
    tag { reads_id }
    module "Python/3.11.5"
    memory "10G"

    input:
    tuple val(reads_id), path(profile) //, path(virus_profiles)

    output:
    path("${reads_id}_aggregated.tsv")

    """
    aggregate_SBG.py ${profile}  ${params.chocophlan_len}  ${reads_id} ${reads_id}_aggregated.tsv
    """
}

process build_matrix {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"

    input:
    path(aggregated) //.toList()

    output:
    path("counts_metaphlan_marker_gene.tsv"),  optional: true
    path("taxonomy_metaphlan_marker_gene.tsv"),  optional: true

    """
    build_matrix.py --taxadb ${params.taxadb} --sgbtaxo ${params.chocophlan_gtdb}  ${aggregated}   counts_metaphlan_marker_gene.tsv taxonomy_metaphlan_marker_gene.tsv
    """
}

process sylph {
    publishDir "$myDir/sylph_profile", mode: 'copy'
    cpus params.cpus
    module "Python/3.11.5"

    input:
    path(reads)

    output:
    tuple path("profiling.tsv"), path("reads.txt"), emit: output

    """
    $baseDir/bin/sylph profile  ${params.sylphdb} -t ${params.cpus} -1 *_R1.fastq.gz -2 *_R2.fastq.gz > profiling.tsv
    r1_files=\$(ls *_R1.fastq.gz)
    countFastq.py \$r1_files reads.txt
    """
}

process sylph_post {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"

    input:
    tuple path(profiling), path(reads)

    output:
    path("counts_sylph.tsv"),  optional: true
    path("taxonomy_sylph.tsv"),  optional: true

    """
    sylph_post.py ${profiling} ${reads} ${params.sylphtaxo} counts_sylph.tsv taxonomy_sylph.tsv
    """
}

process rvdb {
    tag { reads_id }
    module "bowtie2:samtools"
    cpus params.cpus
    memory "20G"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    path("${reads_id}.tsv"),  emit: counts
    path("${reads_id}_cov.tsv"), emit: covs

    """
    bowtie2 -p ${params.cpus} -x ${params.rvdb} -1 ${forward} -2 ${reverse} | samtools view -@ ${params.cpus} -bh - | samtools sort -@ ${params.cpus} -o ${reads_id}.bam
    # Create an index for the BAM file
    samtools index -@ ${params.cpus} ${reads_id}.bam ${reads_id}.bam.bai
    samtools idxstats ${reads_id}.bam > ${reads_id}.tsv
    samtools coverage --reference  ${params.rvdb} -d ${params.depth} -o ${reads_id}_cov.tsv  ${reads_id}.bam
    """
}

process merge_rvdb {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"
    memory "120G"

    input:
    path(counts)
    path(coverage)

    output:
    path("counts_rvdb*.tsv"),  optional: true

    """
    mc=\$(find . -maxdepth 1 -regex '\\./D.*MC[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    mc_cov=\$(find . -name 'D*MC*_cov.tsv')
    ec=\$(find . -maxdepth 1 -regex '\\./D.*EC[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    ec_cov=\$(find . -name 'D*EC*_cov.tsv')
    pv=\$(find . -maxdepth 1 -regex '\\./D.*PV[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    pv_cov=\$(find . -name 'D*PV*_cov.tsv')
    bu=\$(find . -maxdepth 1 -regex '\\./D.*B[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    bu_cov=\$(find . -name 'D*B*_cov.tsv')
    la=\$(find . -maxdepth 1 -regex '\\./D.*L[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    la_cov=\$(find . -name 'D*L*_cov.tsv')

    rvdb_matrix.py --taxadb ${params.taxadb} --cov \$(echo \${mc_cov}) --counts \$(echo \${mc}) --om counts_rvdb_mc.tsv
    rvdb_matrix.py --taxadb ${params.taxadb} --cov \$(echo \${ec_cov}) --counts \$(echo \${ec}) --om counts_rvdb_ec.tsv
    rvdb_matrix.py --taxadb ${params.taxadb} --cov \$(echo \${pv_cov}) --counts \$(echo \${pv}) --om counts_rvdb_pv.tsv
    rvdb_matrix.py --taxadb ${params.taxadb} --cov \$(echo \${bu_cov}) --counts \$(echo \${bu}) --om counts_rvdb_bu.tsv
    rvdb_matrix.py --taxadb ${params.taxadb} --cov \$(echo \${la_cov}) --counts \$(echo \${la}) --om counts_rvdb_la.tsv
    """
}

process humann {
    tag { reads_id }
    memory "40G"
    cpus params.cpus
    module "Python/3.11.5:diamond"


    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    path("${reads_id}_humann/*.tsv"), emit: humann_annotation

    """
    cat ${forward} ${reverse} > ${reads_id}.fastq.gz
    humann -i ${reads_id}.fastq.gz --metaphlan-options \'-x ${params.chocophlan_index_humann} -t rel_ab_w_read_stats --bowtie2db ${params.chocophlan_db} --offline --tmp_dir ${params.tmp}\' --count-normalization Counts --output ${reads_id}_humann --threads ${params.cpus} --remove-temp-output
    """
}

process merge_humann {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"
    memory "20G"

    input:
    path(humann_annotations)

    output:
    path("humann_{pathabundance,reactions}.tsv"), emit: merge_data
    path("humann_genefamilies.tsv")

    """
    humann_join_tables -i ./ -o humann_pathabundance.tsv --file_name pathabundance
    humann_join_tables -i ./ -o humann_genefamilies.tsv --file_name genefamilies
    humann_join_tables -i ./ -o humann_reactions.tsv --file_name reactions
    """
}

process filter_humann {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"
    memory "20G"

    input:
    path(humann_res)

    output:
    path("*_filter.tsv")

    """
    #!/usr/bin/env python3
    from pathlib import Path
    import pandas as pd
    sample = Path("${humann_res}")
    type_sample = sample.stem.split("_")[-1]
    df = pd.read_csv(sample, sep="\\t", header=0, engine="c")
    naughty_list = ['UNMAPPED', 'UNINTEGRATED', 'UNINTEGRATED|unclassified', 'READS_UNMAPPED']
    column_to_remove = 'Counts'
    if column_to_remove in df.columns:
        df.drop(column_to_remove, axis=1, inplace=True)

    df.columns = ["_".join(col.split("_")[:-1]) if col.endswith("_Abundance")  else col for col in df.columns]
    df.columns.values[0] = 'Name'
    df = df[~df['Name'].str.startswith('UNINTEGRATED')]
    df = df[~df['Name'].str.startswith('UNGROUPED')]
    df = df[~df['Name'].isin(naughty_list)]
    df = df.loc[:, (df.sum(axis=0) != 0)]
    row_sums = df.drop('Name', axis=1).sum(axis=1)
    df = df[row_sums != 0]
    df.to_csv(f"{type_sample}_filter.tsv", index=False, sep="\t")
    """
}

process mgv {
    tag { reads_id }
    module "bowtie2:samtools"
    cpus params.cpus
    memory "20G"

    input:
    tuple val(reads_id), path(forward), path(reverse)

    output:
    path("${reads_id}.tsv"),  emit: counts
    path("${reads_id}_cov.tsv"), emit: covs

    """
    bowtie2 -p ${params.cpus} -x ${params.mgv} -1 ${forward} -2 ${reverse} | samtools view -@ ${params.cpus} -bh - | samtools sort -@ ${params.cpus} -o ${reads_id}.bam
    # Create an index for the BAM file
    samtools index -@ ${params.cpus} ${reads_id}.bam ${reads_id}.bam.bai
    samtools idxstats ${reads_id}.bam > ${reads_id}.tsv
    samtools coverage --reference  ${params.mgv} -d ${params.depth} -o ${reads_id}_cov.tsv  ${reads_id}.bam
    """
}

process merge_mgv {
    publishDir "$myDir", mode: 'copy'
    module "Python/3.11.5"
    memory "30G"

    input:
    path(counts)
    path(coverage)

    output:
    path("counts_mgv_*.tsv"),  optional: true

    """
    mc=\$(find . -maxdepth 1 -regex '\\./D.*MC[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    mc_cov=\$(find . -name 'D*MC*_cov.tsv')
    ec=\$(find . -maxdepth 1 -regex '\\./D.*EC[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    ec_cov=\$(find . -name 'D*EC*_cov.tsv')
    pv=\$(find . -maxdepth 1 -regex '\\./D.*PV[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    pv_cov=\$(find . -name 'D*PV*_cov.tsv')
    bu=\$(find . -maxdepth 1 -regex '\\./D.*B[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    bu_cov=\$(find . -name 'D*B*_cov.tsv')
    la=\$(find . -maxdepth 1 -regex '\\./D.*L[0-9]+\\.tsv' ! -regex '.*_cov\\.tsv')
    la_cov=\$(find . -name 'D*L*_cov.tsv')

    mgv_matrix.py --mgv_contig_info ${params.mgv_contig_info} --cov \$(echo \${mc_cov}) --counts \$(echo \${mc}) --om counts_mgv_mc.tsv
    mgv_matrix.py --mgv_contig_info ${params.mgv_contig_info} --cov \$(echo \${ec_cov}) --counts \$(echo \${ec}) --om counts_mgv_ec.tsv
    mgv_matrix.py --mgv_contig_info ${params.mgv_contig_info} --cov \$(echo \${pv_cov}) --counts \$(echo \${pv}) --om counts_mgv_pv.tsv
    mgv_matrix.py --mgv_contig_info ${params.mgv_contig_info} --cov \$(echo \${bu_cov}) --counts \$(echo \${bu}) --om counts_mgv_bu.tsv
    mgv_matrix.py --mgv_contig_info ${params.mgv_contig_info} --cov \$(echo \${la_cov}) --counts \$(echo \${la}) --om counts_mgv_la.tsv
    """
}


workflow {
    readChannel = Channel.fromFilePairs("${params.in}/*_R{1,2}.{fastq,fastq.gz,fq,fq.gz}")
                    .ifEmpty { exit 1, "Cannot find any reads matching: ${params.in}" }
    validate_pair(readChannel)
    reads = validate_pair.out.reads
    alignchocophlan(reads) | metaphlan
    // Marker count path
    aggregate_SBG(metaphlan.out.marker_counts) | collect(flat: false) | build_matrix
    // Classic metaphlan
    metaphlan_relative_abundance = metaphlan.out.relative_abundance.collect(flat: false)
    counts = metaphlan.out.counts.collect(flat: false)
    merge_metaphlan(metaphlan_relative_abundance, counts)
    // Strainphlan path
    compressSAM(alignchocophlan.out.sam)
    compressSAM.out.bz2sam
        .collect()
        .set { strainphlan_input }

    sample2markers(strainphlan_input)

    extract_markers(merge_metaphlan.out.relative_abundance_taxo)
    strainphlan(sample2markers.out.markers, extract_markers.out.markers_fna)

    // Sylph path
    reads
        .map { it -> [it[1], it[2]] }
        .flatten()
        .toList()
        .set { sylph_input }
    sylph(sylph_input)|sylph_post

    // RVDB path
    rvdb(reads)
    counts_rvdb = rvdb.out.counts.collect(flat: false)
    covs_rvdb = rvdb.out.covs.collect(flat: false)
    // merge_rvdb(counts_rvdb, covs_rvdb)

    // MGV path
    mgv(reads)
    counts_mgv = mgv.out.counts.collect(flat: false)
    covs_mgv = mgv.out.covs.collect(flat: false)
    merge_mgv(counts_mgv, covs_mgv)

    // Humann path
    humann(reads)
    humann_annotations = humann.out.humann_annotation.collect(flat: true)
    merge_humann(humann_annotations)
    merged_annotations = merge_humann.out.merge_data.collect()
    filter_humann(merged_annotations.flatten())
}

